import React, { Fragment } from "react";
import "./App.css";
import GetData from "./components/GetData/index";

function App() {
  return (
    <view>
      <GetData />
    </view>
  );
}

export default App;
